***********************************************
Rekomendasi Bahan Belajar Matematika dan Fisika
***********************************************

Buat kalian yang lagi sibuk belajar sains terutama fisika dan matematika, jangan pusing-pusing cari bahan belajar. Ini beberapa rekomendasi yang disarankan untuk kalian.

Apa itu ISENG Belajar?
======================

* ISENG Belajar merupakan website penyedian materi belajar untuk jenjang SMA dan SMP
* Website ini hanya menyediakan materi fisika dan matematika saja.
* Ada juga blog yang tersedia yang bisa kalian baca-baca sambil iseng.

Materi Belajar Fisika
=====================

* `Rangkaian Arus Searah <https://iseng-project.id/materi-fisika/sma/rangkaian-arus-searah/>`_
* `Hukum Newton <https://iseng-project.id/materi-fisika/sma/hukum-newton/>`_
* `Ciri-Ciri Gelombang Mekanik <https://iseng-project.id/materi-fisika/sma/ciri-ciri-gelombang-mekanik/>`_

Materi Belajar Matematika
=========================

* `Konsep Matriks <https://iseng-project.id/materi-matematika/sma/matriks/>`_
* `Induksi Matematika <https://iseng-project.id/materi-matematika/sma/induksi-matematika/>`_
* `Konsep Fungsi <https://iseng-project.id/materi-matematika/sma/fungsi/>`_